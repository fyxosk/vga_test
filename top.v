`default_nettype	none

// look in pins.pcf for all the pin names on the TinyFPGA BX board
module top (
    input CLK,    // 16MHz clock
    output LED,   // User/boot LED next to power LED
    output PIN_13,// RED
    output PIN_12,// GREEN
    output PIN_11,// BLUE
    output PIN_10,// HS
    output PIN_9 ,// VS
    output USBPU  // USB pull-up resistor
);
    // drive USB pull-up resistor to '0' to disable USB
    assign USBPU = 0;

    wire pixel_clk;
    wire clk_locked;

    wire red;
    assign PIN_13 = red;
    wire green;
    assign PIN_12 = green;
    wire blue;
    assign PIN_11 = blue;
    wire hs;
    assign PIN_10 = hs;
    wire vs;
    assign PIN_9  = vs;

    // Use an icepll generated pll
    pll pllpxl( .clock_in(CLK), .clock_out(pixel_clk), .locked( clk_locked ) );

    reg [23:0] ledCounter;
    assign LED = ledCounter[22];

    reg [9:0] h_counter;
    reg [9:0] v_counter;
    wire h_maxed = (h_counter == 10'd799);
    wire v_maxed = (v_counter == 10'd524); 

    reg vga_HS, vga_VS;
    reg inDisplay;

    wire v_blank = (h_counter < 10'd640);

    reg [9:0] carre_x = 10'd160;
    reg [9:0] carre_y = 10'd0;
    reg already_moved, go_down = 1, go_right = 1;

    reg [99:0] sprite [99:0];
    initial begin
    	$readmemb("dvd.txt", sprite, 0, 100);
    end

    wire [0:99] current_line = ((v_counter > carre_y) & (v_counter < carre_y + 10'd100))? sprite[v_counter - carre_y] : 100'd0;

    reg [2:0] couleur;

    always @(posedge pixel_clk) begin
    	if(clk_locked)
        	ledCounter <= ledCounter + 1;
        if(h_maxed) begin
        	h_counter <= 0;
        	if(v_maxed)
        		v_counter <= 0;
        	else
        		v_counter <= v_counter + 1;
        end
        else 
        	h_counter <= h_counter + 1;

        vga_HS <= h_counter[9:7]==3'b101 && h_counter[6:4]!=3'b000 && h_counter[6:4]!=3'b111;
        vga_VS <= v_counter[9:1]==9'd245;
        if (inDisplay == 0) begin
        	inDisplay <= (h_maxed) && (v_counter<480);
        end else 
        	inDisplay <= !(h_counter==639);

        if(!already_moved & ledCounter[16] & v_blank) begin
        	already_moved <= 1;
        	if(go_down) begin
        		if(carre_x == (10'd639 - 10'd100)) begin
        			go_down <= 0;
        			couleur <= couleur + 1;
        		end
        		carre_x <= carre_x + 1;
        	end else begin
        		if(carre_x == 10'd1) begin
        			go_down <= 1;
        			couleur <= couleur + 1;
        		end
        		carre_x <= carre_x - 1;
        	end

        	if(go_right) begin
        		if(carre_y == (10'd479 - 10'd100)) begin
        			go_right <= 0;
        			couleur <= couleur + 1;
        		end
        		carre_y <= carre_y + 1;
        	end else begin
        		if(carre_y == 10'd1) begin
        			go_right <= 1;
        			couleur <= couleur + 1;
        		end
        		carre_y <= carre_y - 1;
        	end

        	if(couleur == 3'b111)
        		couleur <= 3'b000;

        	if(couleur == 3'b000)
        		couleur <= 3'b001;
        end

        if(already_moved & !ledCounter[16])
        	already_moved <= 0;

    end

    wire show_cube = (((h_counter > carre_x) & (h_counter < carre_x + 10'd100) & (v_counter > carre_y) & (v_counter < carre_y + 10'd100) & current_line[h_counter - carre_x]));
    //wire display = inDisplay & (((h_counter > carre_x) & (h_counter < carre_x + 10'd100) & (v_counter > carre_y) & (v_counter < carre_y + 10'd100)) | (h_counter == 0) | (v_counter == 1) | (h_counter == 10'd639) | (v_counter == 10'd480));
    
    assign red   = inDisplay & ((show_cube & !couleur[0]) | (h_counter == 0) | (v_counter == 1) | (h_counter == 10'd639) | (v_counter == 10'd480));
   	assign green = inDisplay & ((show_cube & !couleur[1]) | (h_counter == 0) | (v_counter == 1) | (h_counter == 10'd639) | (v_counter == 10'd480));
    assign blue  = inDisplay & ((show_cube & !couleur[2]) | (h_counter == 0) | (v_counter == 1) | (h_counter == 10'd639) | (v_counter == 10'd480));


    assign hs = ~vga_HS;
    assign vs = ~vga_VS;

endmodule
